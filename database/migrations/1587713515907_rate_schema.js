'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RateSchema extends Schema {
  up () {
    this.create('rates', (table) => {
      table.increments()
      table.integer('announcement_id').unsigned().references('id').inTable('announcements')
      table.string("descripcion")
      table.string("precio")
      table.timestamps()
    })
  }

  down () {
    this.drop('rates')
  }
}

module.exports = RateSchema
