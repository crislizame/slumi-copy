'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProvinceSchema extends Schema {
  up () {
    this.table('provinces', (table) => {
      // alter table
      table.string("slug")
    })
  }

  down () {
    this.table('provinces', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ProvinceSchema
