'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PoblationSchema extends Schema {
  up () {
    this.table('poblations', (table) => {
      // alter table
      table.string("slug")

    })
  }

  down () {
    this.table('poblations', (table) => {
      // reverse alternations
    })
  }
}

module.exports = PoblationSchema
