'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PoblationSchema extends Schema {
  up () {
    this.create('poblations', (table) => {
      table.increments()
      table.string("name")
      table.integer('province_id').unsigned().references('id').inTable('provinces')
      table.timestamps()
    })
  }

  down () {
    this.drop('poblations')
  }
}

module.exports = PoblationSchema
