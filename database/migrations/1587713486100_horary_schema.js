'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class HorarySchema extends Schema {
  up () {
    this.create('horaries', (table) => {
      table.increments()
      table.integer('announcement_id').unsigned().references('id').inTable('announcements')
      table.string("day")
      table.string("desde1")
      table.string("desde2")
      table.string("hasta1")
      table.string("hasta2")
      table.timestamps()
    })
  }

  down () {
    this.drop('horaries')
  }
}

module.exports = HorarySchema
