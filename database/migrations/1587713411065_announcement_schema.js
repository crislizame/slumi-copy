'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AnnouncementSchema extends Schema {
  up () {
    this.create('announcements', (table) => {
      table.increments()
      table.string("telefono")
      table.string("zona").nullable()
      table.string("titulo")
      table.string("texto")
      table.string("nombre")
      table.string("edad")
      table.string("twitter")
      table.string("whatsapp")
      table.string("phoneself")
      table.string("alldays")
      table.string("latitude")
      table.string("magnitude")
      table.timestamps()
    })
  }

  down () {
    this.drop('announcements')
  }
}

module.exports = AnnouncementSchema
