'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EspecialserviceSchema extends Schema {
  up () {
    this.create('especialservices', (table) => {
      table.increments()
      table.integer("parent").nullable()
      table.integer('category_id').unsigned().references('id').inTable('categories').nullable()
      table.string("name")
      table.timestamps()
    })
  }

  down () {
    this.drop('especialservices')
  }
}

module.exports = EspecialserviceSchema
