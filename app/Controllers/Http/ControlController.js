'use strict'

class ControlController {
    async dashboard({ view , request , response ,auth}){
        const user = await auth.getUser()
        return view.render("page.dashboard.index",{user})
    }
    
    async logout({ view , request , response ,auth}){
        await auth.logout()
        let data = {
            ALERT:"Su sesión fue cerrada con éxito"
        }
        return response.json(data)
    }
}

module.exports = ControlController
