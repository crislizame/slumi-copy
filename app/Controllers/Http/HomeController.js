'use strict'
const Mail = use('Mail')
const User = use("App/Models/User")
const Encryption = use('Encryption')
const Env = use('Env')

class HomeController {
    async home({ view , request , response }){

        return view.render("page.home.home")
    }
    
    async control({ view , request , response , auth}){
        try {
            await auth.check()

            return response.redirect("control/dashboard")
        } catch (error) {
            return view.render("page.login.login")
        }
        
    }
    
    async emailpassword({ view , request , response }){
        
        return view.render("page.login.register")
    }
    async restablecerpassword({ view , request , response }){
        const { recordar_email } = request.all()
        var res = {
            "MSG_OK":"",
            "ERRORES":[]
    }
    var user
        const email = recordar_email.split("@")
        const data = {
            email:recordar_email,
            password:"hi1234",
            username:email[0]
        }

        try {
            const usercheck = await User.query().where("email",recordar_email).getCount()
        console.log(usercheck)
        if(usercheck == 0){
             user = await User.create(data)
             console.log(user)
             const hash = Encryption.encrypt(user.id)
            
            const url = Env.get('BASE_URL')+"/"
            await Mail.send('emails.emailpassword', {user,hash,url}, (message) => {
                message
                  .to(recordar_email)
                  .subject('Escoger contraseña Panel de Control')
              })

        }else{
            user = await User.query().where("email",recordar_email).first()
            console.log(user)
            const hash = Encryption.encrypt(user.id)
           
           const url = Env.get('BASE_URL')+"/"
           await Mail.send('emails.restablecerpassword', {user,hash,url}, (message) => {
               message
                 .to(recordar_email)
                 .subject('Restablecer contraseña Panel de Control')
             })
        }
        res.MSG_OK = "/control/restablecer-password"
        } catch (error) {
            console.log(error)
        }
        

        return response.json(res)
    }
    async restablecerpasswordok({view}){
        return view.render("page.login.register")

    }
    async escogerpassword({view, params,request}){
        var res = {
            "MSG_OK":"",
            "ERRORES":[]
    }
         const id = params.id

        return view.render("page.login.escogerpassword",{id})

    }
    async escogerpasswordok({view, params,request,response}){
        var res = {
            "MSG_OK":"",
            "ERRORES":[]
    }
    const { password,password_2,checksum } = request.all()
    const id = Encryption.decrypt(checksum.replace('lizame', '/'))
        if(password === password_2){
            try {
                if(password.length >= 6 && password.length <= 15){
                    
                    const user = await User.find(id)
                    user.password = password
                    await user.save()
                    res.MSG_OK = "/control/password-guardado"

                }else{
                    res.MSG_OK = ""
                    res.ERRORES = {"password":"Debe escribir una contraseña de 6 a 15 caracteres."}
        
                }
            } catch (error) {
                res.MSG_OK = ""
            res.ERRORES = {"password":"No se pudo guardar la contraseña."+error}
            }
        }else{
            res.MSG_OK = ""
            res.ERRORES = {"password":"Las contraseñas no coinciden."}

        }
        return response.json(res)

    }
    async passwordguardado({view}){
        return view.render("page.login.passwordguardado")
    }
}

module.exports = HomeController
