'use strict'
const Province = use('App/Models/Province')
const Category = use('App/Models/Category')
class AnuncioController {
    async altaanuncio({ request, view, response, auth }){

        const provincesall = await Province.query().with('poblation').fetch()
        const provinces = provincesall.toJSON()
        const categoryall = await Category.query().fetch()
        const categories = categoryall.toJSON()
        // return response.json(provinces)
        return view.render('page.anuncio.altaanuncio',{provinces:provinces,'categories':categories})
    }
}

module.exports = AnuncioController
