'use strict'
const User = use("App/Models/User")
class LoginController {
    
    async login({ view , request , response, auth }){
        const { email, password, mantener } = request.all()
        var res = {
                "MSG_OK":"",
                "ERRORES":[]
        }
        try{
            await auth.remember(mantener === "on" ? true : false).attempt(email, password)
            res.MSG_OK = "/control/dashboard"
        }catch(error){
            res.MSG_OK = ""
            res.ERRORES = {"password":"El email o la contraseña no son correctos"}
        }
        return response.json(res)
    }

    async restablecerpassword({view}){
        return view.render('page.login.restablecerpassword')
    }

    

}

module.exports = LoginController
