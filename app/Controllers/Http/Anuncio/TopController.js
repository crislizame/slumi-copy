'use strict'

class TopController {
    async top({ request, view, response, auth }){

        // const provincesall = await Province.query().with('poblation').fetch()
        // const provinces = provincesall.toJSON()

        // return response.json(provinces)
        return view.render('page.dashboard.top')
    }
}

module.exports = TopController
