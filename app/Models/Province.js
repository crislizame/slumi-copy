'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const Poblation = use('App/Models/Poblation')
class Province extends Model {
   
    static boot () {
     
        super.boot()
        // ...
        
        this.addHook('afterFetch', async (province) => {

            console.log(province)

            // await province.poblation()
            // const poblation = await Poblation.query().where('province_id', province.id).fetch()

            // province["poblation"]=poblation

        })
    }
    poblation () {
        return this.hasMany('App/Models/Poblation',"id","province_id")
      }
}

module.exports = Province
