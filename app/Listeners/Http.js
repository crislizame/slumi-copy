'use strict'

const Http = exports = module.exports = {}

Http.method = async () => {
    
}

Http.handleError = function * (error, request, response) {
  
    if (error.status || 401) {
        return response.redirect('/control')
        
      }
    if (error.name === 'InvalidSessionException') {
        return response.redirect('/control')
        
      }
    response.status(error.status).send(error.message) (3)
  }