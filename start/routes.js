'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

// Route.on('/').render('welcome')
Route.get('/','HomeController.home')
Route.get('control','HomeController.control')
Route.get('control/login','HomeController.control')
Route.get('control/email-password','HomeController.emailpassword')
Route.post('control/restablecer-password-ok','HomeController.restablecerpassword')
Route.post('control/restablecer-password','HomeController.restablecerpasswordok')
Route.get('control/escoger-password/:id','HomeController.escogerpassword')
Route.post('control/escoger-password-okx','HomeController.escogerpasswordok')
Route.post('control/password-guardado','HomeController.passwordguardado')
Route.post('control/login-ok','LoginController.login')
Route.get('control/restablecer-password','LoginController.restablecerpassword')
Route.post('control/dashboard','ControlController.dashboard').middleware(['auth'])
Route.get('control/dashboard','ControlController.dashboard').middleware(['auth'])
Route.get('control/logout','ControlController.logout').middleware(['auth'])
Route.get('control/alta-anuncio','AnuncioController.altaanuncio').middleware(['auth'])
Route.get('control/listado-anuncios','Anuncio/ListadoController.listadoanuncios').middleware(['auth'])
Route.get('control/modifica-anuncio','Anuncio/ModificarController.modificaanuncio').middleware(['auth'])
Route.get('control/borrar-anuncio','Anuncio/BorrarController.borraranuncio').middleware(['auth'])
Route.get('control/anuncio-subir-automatico','Anuncio/SubirAutoController.subirautomatico').middleware(['auth'])
Route.get('control/top','Anuncio/TopController.top').middleware(['auth'])
